﻿
using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Serializations
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 1000000;
            F f = new F().Get();

            string str = TestManyTimesSerialization("сериализации ", count, f, Serializer.Serialize);

            object newObj = TestManyTimesDeserialization("обобщенной десериализации", count, str, Serializer.Deserialize<F>);

            newObj = TestManyTimesDeserialization("десериализации c передачей типа", count, str,
                s => Serializer.Deserialize(s, typeof(F)));

            str = TestManyTimesSerialization("сериализации c сохраниением имени типа", count, f, Serializer.SerializeWithTypeName);
            newObj = TestManyTimesDeserialization("десериализации с восстановлением типа", count, str, Serializer.DeserializeWithTypeName);

            str = TestManyTimesSerialization("сериализации json", count, f,JsonConvert.SerializeObject);
            newObj = TestManyTimesDeserialization("десериализации json", count, str, JsonConvert.DeserializeObject<F>);

            MyClass my = new MyClass { DoubleVal = 0.99, str = "test", intVal = 33, boolVal = true, charVal = 'w'};

            str = TestManyTimesSerialization("сериализации ", count, my, Serializer.Serialize);

            newObj = TestManyTimesDeserialization("обобщенной десериализации", count, str, Serializer.Deserialize<MyClass>);

            newObj = TestManyTimesDeserialization("десериализации c передачей типа", count, str,
                s => Serializer.Deserialize(s, typeof(MyClass)));

            str = TestManyTimesSerialization("сериализации c сохраниением имени типа", count, my, Serializer.SerializeWithTypeName);
            newObj = TestManyTimesDeserialization("десериализации с восстановлением типа", count, str, Serializer.DeserializeWithTypeName);

            str = TestManyTimesSerialization("сериализации json", count, my,JsonConvert.SerializeObject);
            newObj = TestManyTimesDeserialization("десериализации json", count, str, JsonConvert.DeserializeObject<MyClass>);
              
        }

        private static object TestManyTimesDeserialization(string actionName, int count, string str, Func<string, object> deserializer)
        {
            object obj = null;
            Stopwatch watch = new Stopwatch();
            Console.WriteLine($"Начинаем {count} циклов {actionName} в новый элемент из сериализованной строки");
            watch.Restart();
            for (int i = 0; i < count; i++)
            {
                obj = deserializer(str);
            }
            watch.Stop();
            Console.WriteLine($"Затраченное время {watch.ElapsedMilliseconds}мс,");
            Console.WriteLine($"Полученный объект {obj}");
            return obj;
        }

        private static string TestManyTimesSerialization(string actionName, int count, object obj, Func<object, string> serializer)
        {
            string str = "";
            Stopwatch watch = new Stopwatch();
            Console.WriteLine($"Начинаем {count} циклов {actionName} с элементом {obj}");
            watch.Start();
            for (int i = 0; i < 100; i++)
            {
                str = serializer(obj);
            }
            watch.Stop();
            Console.WriteLine($"Затраченное время {watch.ElapsedMilliseconds}мс,");
            Console.WriteLine($"Полученная строка  {str}");
            return str;
        }

        class MyClass
        {
            private int prIntVal;

            public int intVal;
            public string str;

            public double DoubleVal { get; set; }
            public bool boolVal{ get; set; }

            public char charVal;

            public override string ToString() => $" field intVal = {intVal}, field str = {str}, propierty DoubleVal = {DoubleVal}, propierty boolVal = {boolVal}, field carVal = {charVal}";
        }
    }

    class F
    {
        public int i1;
        public int i2;
        public int i3;
        public int i4;
        public int i5;

        public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

        public override string ToString() => $" i1 = {i1}, i2 = {i2}, i3 = {i3}, i4 = {i4}, i5 = {i5}";
    }
}

