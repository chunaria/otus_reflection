﻿using System;
using System.Reflection;
using System.Text;

namespace Serializations
{
    public static class Serializer
    {
        private const string separator = ",";
        private const string realComma = "&comma";

        public static string Serialize(Object obj)
        {
            StringBuilder sb = new StringBuilder();
            Type type = obj.GetType();
            foreach (MemberInfo mInfo in type.GetMembers(BindingFlags.Instance | BindingFlags.Public))
            {
                switch (mInfo.MemberType)
                {
                    case MemberTypes.Field:
                        {
                            FieldInfo fInfo = mInfo as FieldInfo;
                            sb.Append(mInfo.Name);
                            sb.Append(separator);
                            sb.Append(fInfo.GetValue(obj).ToString().Replace(separator, realComma));
                            sb.AppendLine();
                            break;
                        }
                    case MemberTypes.Property:
                        {
                            PropertyInfo pInfo = mInfo as PropertyInfo;
                            sb.Append(mInfo.Name);
                            sb.Append(separator);
                            sb.Append(pInfo.GetValue(obj).ToString().Replace(separator, realComma));
                            sb.AppendLine();
                            break;
                        }
                }
            }
            return sb.ToString();
        }

        public static object Deserialize<T>(string serializedStr) where T : new()
        {
            Type type = typeof(T);
            object obj = new T();
            return DeserializeInternal(obj, serializedStr, type) ? obj : null;
        }

        public static object Deserialize(string serializedStr, Type type)
        {
            object obj = Activator.CreateInstance(type);
            return DeserializeInternal(obj, serializedStr, type) ? obj : null;
        }

        private static bool DeserializeInternal(object obj, string serializedStr, Type type)
        {
            string[] memberStrings = serializedStr.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            foreach (string member in memberStrings)
            {
                string[] split = member.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length != 2)
                {
                    Console.WriteLine("something wrong");
                    return false;
                }

                string valueStr = split[1].Replace(realComma, separator);
                PropertyInfo pInfo = type.GetProperty(split[0]);
                if (pInfo != null)
                {
                    if (pInfo.PropertyType == typeof(string))
                    {
                        pInfo.SetValue(obj, valueStr);
                        continue;
                    }
                    object value = Parse(pInfo.PropertyType, valueStr);
                    pInfo.SetValue(obj, value);
                }
                else
                {
                    FieldInfo fInfo = type.GetField(split[0]);
                    if (fInfo != null)
                    {
                        if (fInfo.FieldType == typeof(string))
                        {
                            fInfo.SetValue(obj, valueStr);
                            continue;
                        }
                        object value = Parse(fInfo.FieldType, valueStr);
                        fInfo.SetValue(obj, value);
                    }
                }
            }

            return true;
        }

        private static object Parse(Type type, string str)
        {
            MethodInfo meth = type.GetMethod("TryParse", new Type[] { typeof(string), Type.GetType(string.Format("{0}&", type.FullName)) });
            if (meth != null)
            {
                object[] arguments = new object[] { str, null };
                meth.Invoke(null, arguments);
                return arguments[1];
            }

            return null;
        }

        public static string SerializeWithTypeName(object o)
        {
            Type type = o.GetType();

            return $"typeName,{type.FullName}" + Environment.NewLine + Serialize(o);
        }

        public static object DeserializeWithTypeName(string s)
        {
            int index = s.IndexOf(Environment.NewLine);
            if (index < 0)
            {
                return null;
            }

            string[] typeStr = s.Substring(0, index).Split(separator, StringSplitOptions.RemoveEmptyEntries);
            if (!typeStr[0].Equals("typeName"))
            {
                Console.WriteLine("Неверный формат для данной десериализации");
                return null;
            }

            Type type = Type.GetType(typeStr[1]);
            if (type == null)
            {
                return null;
            }

            Object o = Deserialize(s.Substring(index), type);
            return o;
        }
    }
}
